package com.varunmishra;

import java.io.*;

/**
 * Created by varun on 11/05/15.
 */
public class Hello {
    public static void main(String[] args){
        BufferedReader br = null;
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("text.txt");
        try {


            String sCurrentLine;

            br = new BufferedReader(new InputStreamReader(is));

            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
